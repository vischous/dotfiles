# To get setup
1. Run ./links.sh
2. git clone https://github.com/gmarik/Vundle.vim.git ~/.vim/bundle/Vundle.vim
3. in Vim run :BundleInstall
4. Install node sudo apt install nodejs npm fzf
6. Instlal yarn sudo npm install --global yarn
7. Coc install code dir at ~/vimfiles/bundle/coc.nvim, by running yarn install
1. Run :CocInstall coc-pyright coc-json
1. Run :CocConfig 
1. Set python.venvPath in the json file to "venv / .venv"
10. Set fzf key bindings `source /usr/share/doc/fzf/examples/key-bindings.bash` 
11. After poetry installed run `poetry config virtualenvs.in-project true`
12. git config --global core.editor vim
13. Remove annoying beep sound in WSL, Windows Terminal profile settings, Default Profile, Bell Notification Style
# GPG

To get key forma

1. gpg --list-secret-keys --keyid-format=long . Note that this is the information listed after sec 4096/**** . ***** is the key
2. To let git know about the GPG key. git config --global user.signingkey 616760A7024A1CEB
3. To sign all commits git config --global commit.gpgsign true
4. If you have an error signing you need to set your env varialbe for GPG_TTY `export GPG_TTY=$(tty)`
