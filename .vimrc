" Vundle specefic
set nocompatible              " be iMproved, required
filetype off                  " required

" Dereks
syntax on 
set pastetoggle=<F3>
set nu
set nowrap
" No annoying sound on errors
set noerrorbells
set tabstop=4 softtabstop=4
set vb t_vb=
set expandtab
set smartindent
set shiftwidth=4
set smartcase
set ignorecase
set noswapfile
set nobackup
set undodir=~/.vim/undodir
set undofile
set incsearch
set relativenumber
set colorcolumn=80
set clipboard=unnamedplus
highlight ColorColumn ctermbg=0 guibg=lightgrey

let mapleader = " " " map leader to Space
let g:ftplugin_sql_omni_key = '<Leader>sql'
" :set hlsearch - Highlight search
"set showmatch 

" How many tenths of a second to blink when matching brackets
set mat=2


""""""""""""""""""""""""""""""
" => Status line
""""""""""""""""""""""""""""""
" Always show the status line
set laststatus=2

" Format the status line
" set statusline=\ %{HasPaste()}%F%m%r%h\ %w\ \ CWD:\ %r%{getcwd()}%h\ \ \ Line:\ %l\ \ Column:\ %c


" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin('~/vimfiles/bundle')
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'

"Plugins
Plugin 'morhetz/gruvbox'
Plugin 'neoclide/coc.nvim'
Plugin 'puremourning/vimspector'
Plugin 'junegunn/fzf'
Plugin 'tpope/vim-fugitive'
"Plugin 'valloric/youcompleteme'
"https://www.reddit.com/r/vim/comments/2om1ib/how_to_disable_sql_dynamic_completion/
"if I start having issues with SQL again

" The following are examples of different formats supported.
" Keep Plugin commands between vundle#begin/end.
" plugin on GitHub repo
" Plugin 'tpope/vim-fugitive'
" plugin from http://vim-scripts.org/vim/scripts.html
" Plugin 'L9'
" Git plugin not hosted on GitHub
" Plugin 'git://git.wincent.com/command-t.git'
" git repos on your local machine (i.e. when working on your own plugin)
" Plugin 'file:///home/gmarik/path/to/plugin'
" The sparkup vim script is in a subdirectory of this repo called vim.
" Pass the path to set the runtimepath properly.
" Plugin 'rstacruz/sparkup', {'rtp': 'vim/'}
" Install L9 and avoid a Naming conflict if you've already installed a
" different version somewhere else.
" Plugin 'ascenator/L9', {'name': 'newL9'}

" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required
" To ignore plugin indent changes, instead use:
"filetype plugin on
"
" Brief help
" :PluginList       - lists configured plugins
" :PluginInstall    - installs plugins; append `!` to update or just :PluginUpdate
" :PluginSearch foo - searches for foo; append `!` to refresh local cache
" :PluginClean      - confirms removal of unused plugins; append `!` to auto-approve removal
"
" see :h vundle for more details or wiki for FAQ
" Put your non-Plugin stuff after this line
" Gruvbox specefic
"set termguicolors
set background=dark
colorscheme gruvbox
set termguicolors
" CocSpecefic

"Default is 4s this leads to poor user experiance
set updatetime=300
" GoTo code navigation.
nmap <silent> gd <Plug>(coc-definition)
nmap <silent> gy <Plug>(coc-type-definition)
nmap <silent> gi <Plug>(coc-implementation)
nmap <silent> gr <Plug>(coc-references)
nmap <silent> poi <Plug>(coc-config-suggest)
inoremap <silent><expr> <c-@> coc#refresh
inoremap <expr> <Tab> pumvisible() ? "\<C-n>" : "\<Tab>"
inoremap <expr> <S-Tab> pumvisible() ? "\<C-p>" : "\<S-Tab>"

"VimSpector
let g:vimspector_enable_mappings = 'HUMAN'
inoremap <silent><expr> <c-space> coc#refresh()

"FZF searcher
nnoremap <silent> <C-f> :FZF<CR>

"VimFugitive
"https://medium.com/prodopsio/solving-git-merge-conflicts-with-vim-c8a8617e3633
"leader merge conflicts split
nnoremap <leader>mcs :Gvdiffsplit!<CR> 
 "leader merge conflicts left
nnoremap <leader>mch :diffget //2<CR>
 "leader merge conflicts right
nnoremap <leader>mcl :diffget //3<CR>
"Other nice short cuts when dealing with merge conflicts [c for backwards ]c
"forward (next githunk to fix"

"Nice terminal colors
let g:terminal_ansi_colors = [
  \'#eeeeee', '#af0000', '#008700', '#5f8700',
  \'#0087af', '#878787', '#005f87', '#444444',
  \'#bcbcbc', '#d70000', '#d70087', '#8700af',
  \'#d75f00', '#d75f00', '#005faf', '#005f87' ]
